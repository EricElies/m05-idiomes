package com.company;

public class Textos {
    private String diesLaborals1CA = "La setmana te ";
    private String diesLaborals2CA = "dies laborals.";

    private String diesLaborals1ES = "La semana tiene ";
    private String diesLaborals2ES = "dias laborales.";

    private String diesLaborals1EN = "The week has ";
    private String diesLaborals2EN = "working days.";

    private static String IDIOMA_NO_CONTROLAT = "Idioma no controlat";


    public String getDiesLaborals1CA() {
        return diesLaborals1CA;
    }

    public String getDiesLaborals2CA() {
        return diesLaborals2CA;
    }

    public String getDiesLaborals1ES() {
        return diesLaborals1ES;
    }

    public String getDiesLaborals2ES() {
        return diesLaborals2ES;
    }

    public String getDiesLaborals1EN() {
        return diesLaborals1EN;
    }

    public String getDiesLaborals2EN() {
        return diesLaborals2EN;
    }

    public String fraseDiesLaborals(String idioma, int diesLaborals){
        String frase = IDIOMA_NO_CONTROLAT;


        switch (idioma) {
            case Constants.ESPANYOL:
                frase = diesLaborals1ES + diesLaborals + diesLaborals2ES;
                break;
            case Constants.CATALA:
                frase = diesLaborals1CA + diesLaborals + diesLaborals2CA;
                break;
            case Constants.INGLES:
                frase = diesLaborals1EN + diesLaborals + diesLaborals2EN;
                break;
            default:
                break;
        }
        return frase;
    }
}
