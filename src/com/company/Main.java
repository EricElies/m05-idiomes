package com.company;

import java.util.Arrays;

public class Main {

    static final int DIES_SETMANA = 7;
    static final int DIES_LABORABLES = 5;
    static Textos textos;

    public static void main(String[] args) {
	// write your code here
        System.out.println("args = " + Arrays.deepToString(args));
        System.out.println("DSETMANA = " + DIES_SETMANA);
        System.out.println("DIES_LABORABLES = " + DIES_LABORABLES);
        System.out.println("-----------------------------------");

        textos = new Textos();

        for (int i=0; i< args.length; i++){
            System.out.println("\n* " + args[i] +":");
            switch (args[i]) {
                case Constants.CATALA:
                    System.out.println(textos.getDiesLaborals1CA() + DIES_LABORABLES + Constants.ESPAI+textos.getDiesLaborals2CA());
                    break;
                case Constants.ESPANYOL:
                    System.out.println(textos.getDiesLaborals1ES() + DIES_LABORABLES + Constants.ESPAI+textos.getDiesLaborals2ES());
                    break;
                case Constants.INGLES:
                    System.out.println(textos.getDiesLaborals1EN() + DIES_LABORABLES + Constants.ESPAI+textos.getDiesLaborals2EN());
                    break;
                default:
                    System.out.println("[!] Idioma no reconegut");
            }
        }

        System.out.println("\n--------- UNA ALTRA OPCIÓ: ---------\n");

        for (int i=0; i< args.length; i++){
            System.out.println("\n* " + args[i] +":");
            System.out.println(textos.fraseDiesLaborals(args[i], DIES_LABORABLES));
        }

    }
}
